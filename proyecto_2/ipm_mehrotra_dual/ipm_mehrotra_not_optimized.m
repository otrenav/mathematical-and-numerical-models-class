%
% Controlador del método de puntos interiores de Mehrotra
% para máquinas de soporte vectorial sobre disntintos datos.
%
% Este código no está optimizado para aprovechar la
% característica sparse de las matrices.
%
% Este código utilizó como base el código de Omar Pardo.
%
% Omar Trejo
% 119711
%
% Prof. José Luis Morales
% ITAM, 2015
%
function [x, lambda, s] = ipm_mehrotra_not_optimized (A, b, gamma, TOL);
    [n, m] = size(A);

    % Punto inicial
    e = ones(m,1);
    x = (gamma/2)*e; 
    s = x;
    y = A*x;
    lambda = 1;

    % Matrices auxiliares
    X = diag(x); 
    X = sparse(X);
    X_1 = diag(1./x); 
    X_1 = sparse(X_1);
    S = diag(s); 
    S = sparse(S);
    S_1 = diag(1./s); 
    S_1 = sparse(S_1);

    % Definimos z y w
    z = x;
    w = x;

    % Matrices auxiliares
    Z = diag(z); Z = sparse(Z);
    W = diag(w); W = sparse(W);

    % Brecha inicial
    mu = (x'*z+s'*w)/(2*m);

    % Vector de F's y tau
    F = zeros(n+m+1);
    tau = 0.9995d0;

    % Condiciones de F, sin tomar en cuenta mu
    F1 = -e-z-lambda*b+A'*y+w;
    F2 = b'*x;
    F3 = A*x-y;
    F4 = x-gamma*e+s;
    F5 = X*z;
    F6 = S*w;
    F1_bis = F1+X_1*F5-S_1*F6+S_1*W*F4;

    % Normas de F
    F1_n = norm(F1);
    F2_n = norm(F2);
    F3_n = norm(F3);
    F4_n = norm(F4);
    F5_n = norm(F5);
    F6_n = norm(F6);

    % Función objetivo y brecha inicial
    f_objetivo =  (0.5)*y'*y-e'*x;
    d_gap = mu;

    % Vector inicial F
    F   = -[ F1_bis; F3; -F2 ]; F_n = norm(F); iter = 0;

    fprintf('\n');
    fprintf(['iter d_gap    f_obj     ' ...
             '||F1||   ||F2||   ||F3||   ||F4||   ||F5||   ||F6||   alpha    sigma    tau\n']);
    fprintf(['---------------------------------------------------' ...
            '----------------------------------------------------\n']);
    fprintf('%4i %8.2e %+8.2e %8.2e %8.2e %8.2e %8.2e %8.2e %8.2e \n', ...
            iter, d_gap, f_objetivo, F1_n, F2_n, F3_n, F4_n, F5_n, F6_n);

    while d_gap >  TOL & iter < 30 
        iter = iter + 1;    
        % Jacobiana
        KKT = [   X_1*Z + S_1*W         A'         -b     ;
                  A            -eye(n)     zeros(n,1);  
                  -b'           zeros(1,n)     0     ];              
        KKT = sparse(KKT);
        
        % Resolvemos el sistema
        [L, D, P, S] = ldl(KKT);
        
        dt = backsolve( L, D, P, S, F );
        dx = dt(1:m);
        dy = dt(m+1:m+n);
        dlambda = dt(m+n+1);
        ds = -F4-dx;
        dz = -X_1*(F5+Z*dx);
        dw = -S_1*(F6+W*ds);
        
        alpha_x = step_d ( x, dx, 1 );
        alpha_s = step_d ( s, ds, 1 );
        alpha_z = step_d ( z, dz, 1 );
        alpha_w = step_d ( w, dw, 1 );
        
        % Parámetro de centrado sigma
        mu_aff = ((x + alpha_x*dx)'*(z + alpha_z*dz) + ...
                  (s + alpha_s*ds)'*(w + alpha_w*dw))/(2*m);
        sigma  = (mu_aff/mu)^3;
        
        F5 = F5+dx.*dz-sigma*mu*e;
        F6 = F6+ds.*dw-sigma*mu*e;
        F1_bis = F1+X_1*F5-S_1*F6+S_1*W*F4;
        F   = -[ F1_bis; F3; -F2 ];  
        
        % Calculamos el paso corrector
        dt = backsolve( L, D, P, S, F );
        dx = dt(1:m);
        dy = dt(m+1:m+n);
        dlambda = dt(m+n+1);
        
        ds = -F4-dx;
        dz = -X_1*(F5+Z*dx);
        dw = -S_1*(F6+W*ds);
        
        alpha_x = step_d ( x, dx, tau );
        alpha_s = step_d ( s, ds, tau );
        alpha_z = step_d ( z, dz, tau );
        alpha_w = step_d ( w, dw, tau );
        alpha = min(alpha_x, alpha_s);

        % Actualización de paso
        x = x + alpha_x*dx;  
        y = y + alpha*dy;
        lambda = lambda + alpha*dlambda;
        s = s + alpha_s*ds;
        z = z + alpha_z*dz;
        w = w + alpha_w*dw;
        
        % Recalculamos valores
        X = diag(x); X = sparse(X);
        X_1 = diag(e./x); X_1 = sparse(X_1);
        S = diag(s); S = sparse(S);
        S_1 = diag(e./s); S_1 = sparse(S_1);
        Z = diag(z); Z = sparse(Z);
        W = diag(w); W = sparse(W);
        
        mu = (x'*z+s'*w)/(2*m);       
        d_gap  = mu;
        
        % Recalculamos condiciones de F
        F1 = -e-z-lambda*b+A'*y+w;
        F2 = b'*x;
        F3 = A*x-y;
        F4 = x-gamma*e+s;
        F5 = X*z;
        F6 = S*w;
        F1_bis = F1+X_1*F5-S_1*F6+S_1*W*F4;
        F = -[F1_bis; F3; -F2];  
        F_norm = norm(F);
        
        % Calculamos las normas de F
        F1_n = norm(F1);
        F2_n = norm(F2);
        F3_n = norm(F3);
        F4_n = norm(F4);
        F5_n = norm(F5);
        F6_n = norm(F6);

        % Función objetivo
        f_objetivo =  (0.5)*y'*y-e'*x;
        
        fprintf('%4i %8.2e %+8.2e %8.2e %8.2e %8.2e %8.2e %8.2e %8.2e %8.2e %8.2e %8.2e \n', ...
                iter, d_gap, f_objetivo, F1_n, F2_n, F3_n, F4_n, F5_n, F6_n, ...
                alpha, sigma, tau);
    end
end
