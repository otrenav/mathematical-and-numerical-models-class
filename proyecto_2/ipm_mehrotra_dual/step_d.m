%
% Calcular alpha par el paso 
%
function alpha = step_d (x, dx, tau);
    one = 1.0d0; 
    zero = 0.0d0;
    ind = find( dx<zero );
    if isempty(ind) == 1
        alpha = tau;
    else
        alpha = min(-x(ind)./dx(ind));
        alpha = tau*min (one, alpha);
    end
end
