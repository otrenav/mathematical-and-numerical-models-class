%
% Controlador del método de puntos interiores de Mehrotra
% para máquinas de soporte vectorial sobre disntintos datos.
%
% Omar Trejo
% 119711
%
% Prof. José Luis Morales
% ITAM, 2015
%
function [x, lambda, s] = SVM_ipm_mehrotra_dual(data_set);
    if nargin < 1 || strcmp(data_set, 'wdbc')
        data_set = 'wdbc';
        [T, ~, ~, ~] = wdbcData('./data/wdbc.data', 30, 0.0, 1);
    elseif strcmp(data_set, 'forest_fires')
        T = csvread('./data/forest_fires/forest_fires_clean.csv');
    elseif strcmp(data_set, 'white_wines')
        T = csvread('./data/wine_quality/white_wines_quality_clean.csv');
    else
        error(['You must specify a data set (wdbc, forest_fires, ' ...
               'or white_wines) as a string.'])
    end

    tolerance = 1.0e-8;

    % Obtenemos las dimensiones 
    % y quitamos el identificador
    [n_row, n_col] = size(T);
    n_atr = n_col - 1;
    X = T(1:n_row, 2:n_atr + 1);

    % Escalamos la matriz y la transponemos 
    % para dejarla en términos del modelo
    X = scale(X);
    X = X';

    % Reetiquetamos los 
    % valores que tenían 0
    ind = T(:, 1) ~= 1;
    T(ind, 1) = -1;

    % Definimos las matrices 
    % de acuerdo al modelo
    b = T(:, 1);
    Y = diag(b);

    A = X*Y;
    gamma = 1000;

    fprintf('\n');
    fprintf('   Data set            .......... %s\n', data_set);
    fprintf('   Number of samples   .......... %3i\n', n_row);
    fprintf('   Number of atributes .......... %2i\n', n_atr);
    fprintf('   Tolerance           .......... %3i\n', tolerance);

    tic;
    % [x, lambda, s] = ipm_mehrotra_not_optimized(A, b, gamma, tolerance);
    [x, lambda, s] = ipm_mehrotra_optimized(A, b, gamma, tolerance);
    toc;
end