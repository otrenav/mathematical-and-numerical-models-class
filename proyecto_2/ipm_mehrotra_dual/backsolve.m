%
% Resolver hacia atr'as un sistema
%
function [d] = backsolve (L, D, P, S, F);
    d = P'*(S*F);
    d = L\d;
    d = D\d;
    d = L'\d;
    d = S*P*d;
end
