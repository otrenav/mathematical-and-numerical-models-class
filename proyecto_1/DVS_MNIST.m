%
% Proyecto MNIST
%
% Omar Trejo Navarro - 119711
% Prof. José Luis Morales
% ITAM, 2015
%
% La matriz X contiene 10 bloques de 500x500 unidos lateralmente.
% El primer bloque contiene la muestra correspondiente al '0', el
% segundo bloque la muestra correspondiente al '1', y así sucesivamente.
%
% El objetivo es clasificar los digitos de la mejor manera 
% posible utilizando la DVS y resolviendo un problema de MC.
%

load('data_numbers.mat');
X = X';

%
% Parámetros
%
I = eye(400);
U = zeros(400, 4000);
numero_de_digitos = 10;
numero_de_pruebas = 1000;
numero_max_de_vectores = 100;
digitos_reales = zeros(numero_de_pruebas, 1);
posiciones_elegidas = zeros(numero_de_pruebas, 1);
tasas_de_acierto = zeros(numero_max_de_vectores, 1);
digitos_identificados = zeros(numero_de_pruebas, 1);

%
% Calcular las DVS para cada dígito
%
% U es de 400x4000. Cada bloque de 400x400 corresponde
% a la U de la DVS de un dígito distinto y aparecen en 
% el mismo orden que los de la matriz X (de 0 a 9).
%
for i = 1:numero_de_digitos
    [U(:, ((i - 1)*400 + 1):(i*400)), ~, ~] = svd(X(:, ((i - 1)*500 + 1):(i*500)));
end

%
% Procedimiento de prueba
%
for k = 1:numero_max_de_vectores
    fprintf('[+] Calculando con k = %d (%d/%d)\n', k, k, numero_max_de_vectores);
    for i = 1:numero_de_pruebas
        z = randi(5000, 1);
        posiciones_elegidas(i) = z;
        digitos_reales(i) = floor(z/500);
        residuales = zeros(numero_de_digitos, 1);
        for j = 1:numero_de_digitos
            residuales(j) = norm((I - ...
                                  U(:, ((j - 1)*400 + 1):((j - 1)*400 + k))* ...
                                  U(:, ((j - 1)*400 + 1):((j - 1)*400 + k))')* ...
                                  X(:, z));
        end
        [~, digitos_identificados(i)] = min(residuales);
    end
    aciertos = digitos_reales == (digitos_identificados - 1);
    tasas_de_acierto(k) = sum(aciertos)/numero_de_pruebas;
end

%
% Resultados
%
tasas_de_error = 1 - tasas_de_acierto
plot(1:numero_max_de_vectores, tasas_de_error, 'k', 'LineWidth', 2);
title('Tasas de error respecto al numero de vectores propios incluidos', ...
      'FontSize', 14);
xlabel('Numero de vectores propios incluidos (en orden)', ...
       'FontSize', 12);
ylabel('Tasa de error porcentual', ...
       'FontSize', 12);