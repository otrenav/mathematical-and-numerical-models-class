
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Mathematical and numerical models

- Omar Trejo
- August, 2015

This code was developed for the "Mathematical and numerical models" class I
attended at ITAM in 2015. The class was a class regarding optimization
algorithms for Machine Learning. It was taught by Dr. José Luis Morales.

A related repository is the [Sequential Minimal Optimization poster](https://github.com/otrenav/smo_poster).

Any feedback is welcome!

---

> "The best ideas are common property."
>
> —Seneca
